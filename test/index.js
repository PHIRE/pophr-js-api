fetch = require('node-fetch');
const pophr = require('../dist/bundle.js');

const PopHRApi = pophr.API;

const server = new PopHRApi('http://pophr-dev.mchi.mcgill.ca/pophr', "-- bearer token --");

const conn = server.connect()
  .then(() => console.log('server connection successful'))
  .catch(() => console.log('server connection failed'));

const ind = 'phio:PopulationCount';
const stratifications = ['phio:stratification_CLSC'];

conn.then(() => server.getIndicator({ ind: ind, str: stratifications }))
  .then(response => console.info('indicator data', response))
  .catch(error => console.error('indicator error', error));