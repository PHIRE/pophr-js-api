module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/impl.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/impl.ts":
/*!*********************!*\
  !*** ./src/impl.ts ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class API {
    constructor(baseUrl, bearerToken) {
        this.baseUrl = baseUrl;
        this.bearerToken = bearerToken;
    }
    get options() { return { headers: { Authorization: this.bearerToken } }; }
    // Base
    close() { this.isConnected = false; }
    connect() { return this.isConnected ? Promise.resolve() : fetch(`${this.baseUrl}/connection`).then(() => { this.isConnected = true; }); }
    // Analysis
    getCausesOfDeaths(stratifications, filters, yearWindow) { return Promise.resolve(null); }
    getCorrelation(srcIndicator, dstIndicator) { return Promise.resolve(null); }
    // App
    listExperiments() { return Promise.resolve(null); }
    listRoles() { return Promise.resolve(null); }
    // Discovery
    getCategorizedConceptsByFramework(framework) { return Promise.resolve(null); }
    getConceptDetails(indicator) { return Promise.resolve(null); }
    getConceptSynonyms(ids) { return Promise.resolve(null); }
    listDimensions() { return Promise.resolve(null); }
    // Docs
    getByHash(hash) { return Promise.resolve(null); }
    getLatest(shortForm, locale) { return Promise.resolve(null); }
    listDocuments() { return Promise.resolve(null); }
    // Graph
    getGraph() { return Promise.resolve(null); }
    // Indicator
    getIndicator({ ind, str = [], fil = [], std = [] }) {
        console.assert(this.isConnected, "getIndicator : server is not connected");
        return fetch(`${this.baseUrl}/indicator/${ind}?${[].concat(fil.map(s => `f=${s}`), str.map(s => `d=${s}`), std.map(s => `std=${s}`)).join("&")}`, this.options)
            .then(res => res.json())
            .then(json => json['content']['data']);
    }
    // Suggestion
    getDefaultConcept() { return Promise.resolve(null); }
    listConcepts() { return Promise.resolve(null); }
    // User
    getUserByUID(uid) { return Promise.resolve(null); }
}
exports.API = API;


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9wb3Boci93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly9wb3Boci8uL3NyYy9pbXBsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxrREFBMEMsZ0NBQWdDO0FBQzFFO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZ0VBQXdELGtCQUFrQjtBQUMxRTtBQUNBLHlEQUFpRCxjQUFjO0FBQy9EOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpREFBeUMsaUNBQWlDO0FBQzFFLHdIQUFnSCxtQkFBbUIsRUFBRTtBQUNySTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOzs7QUFHQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7QUM5RUEsTUFBYSxHQUFHO0lBTWQsWUFBWSxPQUFlLEVBQUUsV0FBbUI7UUFDOUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7UUFDdkIsSUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUM7SUFDakMsQ0FBQztJQUVELElBQUksT0FBTyxLQUFhLE9BQU8sRUFBRSxPQUFPLEVBQUUsRUFBRSxhQUFhLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBRWxGLE9BQU87SUFDUCxLQUFLLEtBQUssSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQ3JDLE9BQU8sS0FBb0IsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLGFBQWEsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUV4SixXQUFXO0lBQ1gsaUJBQWlCLENBQUMsZUFBeUIsRUFBRSxPQUFpQixFQUFFLFVBQWtCLElBQW9CLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDckksY0FBYyxDQUFDLFlBQW9CLEVBQUUsWUFBb0IsSUFBb0IsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUU1RyxNQUFNO0lBQ04sZUFBZSxLQUFxQixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ25FLFNBQVMsS0FBcUIsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUU3RCxZQUFZO0lBQ1osaUNBQWlDLENBQUMsU0FBaUIsSUFBb0IsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN0RyxpQkFBaUIsQ0FBQyxTQUFpQixJQUFvQixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3RGLGtCQUFrQixDQUFDLEdBQWEsSUFBb0IsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNuRixjQUFjLEtBQXFCLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFFbEUsT0FBTztJQUNQLFNBQVMsQ0FBQyxJQUFZLElBQWtCLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDdkUsU0FBUyxDQUFDLFNBQWlCLEVBQUUsTUFBYyxJQUFrQixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzVGLGFBQWEsS0FBcUIsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUVqRSxRQUFRO0lBQ1IsUUFBUSxLQUFxQixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBRTVELFlBQVk7SUFDWixZQUFZLENBQUMsRUFBRSxHQUFHLEVBQUUsR0FBRyxHQUFHLEVBQUUsRUFBRSxHQUFHLEdBQUcsRUFBRSxFQUFFLEdBQUcsR0FBRyxFQUFFLEVBQWdFO1FBQzlHLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSx3Q0FBd0MsQ0FBQyxDQUFDO1FBRTNFLE9BQU8sS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sY0FBYyxHQUFHLElBQUksRUFBRSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEVBQUUsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUM7YUFDMUosSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDO2FBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUFFRCxhQUFhO0lBQ2IsaUJBQWlCLEtBQXFCLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDckUsWUFBWSxLQUFxQixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBRWhFLE9BQU87SUFDUCxZQUFZLENBQUMsR0FBVyxJQUFvQixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO0NBQzVFO0FBdERELGtCQXNEQyIsImZpbGUiOiJidW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9pbXBsLnRzXCIpO1xuIiwiZGVjbGFyZSBmdW5jdGlvbiBmZXRjaCh1cmw6IFN0cmluZywgb3B0aW9ucz86IE9iamVjdCk6IFByb21pc2U8YW55PjtcclxuXHJcbmltcG9ydCB7IElBUEksIENQYWNrLCBQREYgfSBmcm9tIFwiLi9pbmRleFwiO1xyXG5cclxuZXhwb3J0IGNsYXNzIEFQSSBpbXBsZW1lbnRzIElBUEkge1xyXG5cclxuICBiYXNlVXJsOiBTdHJpbmc7XHJcbiAgYmVhcmVyVG9rZW46IFN0cmluZztcclxuICBpc0Nvbm5lY3RlZDogYm9vbGVhbjtcclxuXHJcbiAgY29uc3RydWN0b3IoYmFzZVVybDogU3RyaW5nLCBiZWFyZXJUb2tlbjogU3RyaW5nKSB7XHJcbiAgICB0aGlzLmJhc2VVcmwgPSBiYXNlVXJsO1xyXG4gICAgdGhpcy5iZWFyZXJUb2tlbiA9IGJlYXJlclRva2VuO1xyXG4gIH1cclxuXHJcbiAgZ2V0IG9wdGlvbnMoKTogT2JqZWN0IHsgcmV0dXJuIHsgaGVhZGVyczogeyBBdXRob3JpemF0aW9uOiB0aGlzLmJlYXJlclRva2VuIH0gfTsgfVxyXG5cclxuICAvLyBCYXNlXHJcbiAgY2xvc2UoKSB7IHRoaXMuaXNDb25uZWN0ZWQgPSBmYWxzZTsgfVxyXG4gIGNvbm5lY3QoKTogUHJvbWlzZTx2b2lkPiB7IHJldHVybiB0aGlzLmlzQ29ubmVjdGVkID8gUHJvbWlzZS5yZXNvbHZlKCkgOiBmZXRjaChgJHt0aGlzLmJhc2VVcmx9L2Nvbm5lY3Rpb25gKS50aGVuKCgpID0+IHsgdGhpcy5pc0Nvbm5lY3RlZCA9IHRydWU7IH0pOyB9XHJcblxyXG4gIC8vIEFuYWx5c2lzXHJcbiAgZ2V0Q2F1c2VzT2ZEZWF0aHMoc3RyYXRpZmljYXRpb25zOiBTdHJpbmdbXSwgZmlsdGVyczogU3RyaW5nW10sIHllYXJXaW5kb3c6IG51bWJlcik6IFByb21pc2U8Q1BhY2s+IHsgcmV0dXJuIFByb21pc2UucmVzb2x2ZShudWxsKTsgfVxyXG4gIGdldENvcnJlbGF0aW9uKHNyY0luZGljYXRvcjogU3RyaW5nLCBkc3RJbmRpY2F0b3I6IFN0cmluZyk6IFByb21pc2U8Q1BhY2s+IHsgcmV0dXJuIFByb21pc2UucmVzb2x2ZShudWxsKTsgfVxyXG5cclxuICAvLyBBcHBcclxuICBsaXN0RXhwZXJpbWVudHMoKTogUHJvbWlzZTxDUGFjaz4geyByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKG51bGwpOyB9XHJcbiAgbGlzdFJvbGVzKCk6IFByb21pc2U8Q1BhY2s+IHsgcmV0dXJuIFByb21pc2UucmVzb2x2ZShudWxsKTsgfVxyXG5cclxuICAvLyBEaXNjb3ZlcnlcclxuICBnZXRDYXRlZ29yaXplZENvbmNlcHRzQnlGcmFtZXdvcmsoZnJhbWV3b3JrOiBTdHJpbmcpOiBQcm9taXNlPENQYWNrPiB7IHJldHVybiBQcm9taXNlLnJlc29sdmUobnVsbCk7IH1cclxuICBnZXRDb25jZXB0RGV0YWlscyhpbmRpY2F0b3I6IFN0cmluZyk6IFByb21pc2U8Q1BhY2s+IHsgcmV0dXJuIFByb21pc2UucmVzb2x2ZShudWxsKTsgfVxyXG4gIGdldENvbmNlcHRTeW5vbnltcyhpZHM6IFN0cmluZ1tdKTogUHJvbWlzZTxDUGFjaz4geyByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKG51bGwpOyB9XHJcbiAgbGlzdERpbWVuc2lvbnMoKTogUHJvbWlzZTxDUGFjaz4geyByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKG51bGwpOyB9XHJcblxyXG4gIC8vIERvY3NcclxuICBnZXRCeUhhc2goaGFzaDogU3RyaW5nKTogUHJvbWlzZTxQREY+IHsgcmV0dXJuIFByb21pc2UucmVzb2x2ZShudWxsKTsgfVxyXG4gIGdldExhdGVzdChzaG9ydEZvcm06IFN0cmluZywgbG9jYWxlOiBTdHJpbmcpOiBQcm9taXNlPFBERj4geyByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKG51bGwpOyB9XHJcbiAgbGlzdERvY3VtZW50cygpOiBQcm9taXNlPENQYWNrPiB7IHJldHVybiBQcm9taXNlLnJlc29sdmUobnVsbCk7IH1cclxuXHJcbiAgLy8gR3JhcGhcclxuICBnZXRHcmFwaCgpOiBQcm9taXNlPENQYWNrPiB7IHJldHVybiBQcm9taXNlLnJlc29sdmUobnVsbCk7IH1cclxuXHJcbiAgLy8gSW5kaWNhdG9yXHJcbiAgZ2V0SW5kaWNhdG9yKHsgaW5kLCBzdHIgPSBbXSwgZmlsID0gW10sIHN0ZCA9IFtdIH06IHsgaW5kOiBTdHJpbmcsIHN0cjogU3RyaW5nW10sIGZpbDogU3RyaW5nW10sIHN0ZDogU3RyaW5nW10gfSk6IFByb21pc2U8Q1BhY2s+IHtcclxuICAgIGNvbnNvbGUuYXNzZXJ0KHRoaXMuaXNDb25uZWN0ZWQsIFwiZ2V0SW5kaWNhdG9yIDogc2VydmVyIGlzIG5vdCBjb25uZWN0ZWRcIik7XHJcblxyXG4gICAgcmV0dXJuIGZldGNoKGAke3RoaXMuYmFzZVVybH0vaW5kaWNhdG9yLyR7aW5kfT8ke1tdLmNvbmNhdChmaWwubWFwKHMgPT4gYGY9JHtzfWApLCBzdHIubWFwKHMgPT4gYGQ9JHtzfWApLCBzdGQubWFwKHMgPT4gYHN0ZD0ke3N9YCkpLmpvaW4oXCImXCIpfWAsIHRoaXMub3B0aW9ucylcclxuICAgICAgICAudGhlbihyZXMgPT4gcmVzLmpzb24oKSlcclxuICAgICAgICAudGhlbihqc29uID0+IGpzb25bJ2NvbnRlbnQnXVsnZGF0YSddKTtcclxuICB9XHJcblxyXG4gIC8vIFN1Z2dlc3Rpb25cclxuICBnZXREZWZhdWx0Q29uY2VwdCgpOiBQcm9taXNlPENQYWNrPiB7IHJldHVybiBQcm9taXNlLnJlc29sdmUobnVsbCk7IH1cclxuICBsaXN0Q29uY2VwdHMoKTogUHJvbWlzZTxDUGFjaz4geyByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKG51bGwpOyB9XHJcblxyXG4gIC8vIFVzZXJcclxuICBnZXRVc2VyQnlVSUQodWlkOiBTdHJpbmcpOiBQcm9taXNlPENQYWNrPiB7IHJldHVybiBQcm9taXNlLnJlc29sdmUobnVsbCk7IH1cclxufSJdLCJzb3VyY2VSb290IjoiIn0=