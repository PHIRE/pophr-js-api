declare function fetch(url: String, options?: Object): Promise<any>;

import { IAPI, CPack, PDF } from "./index";

export class API implements IAPI {

  baseUrl: String;
  bearerToken: String;
  isConnected: boolean;

  constructor(baseUrl: String, bearerToken: String) {
    this.baseUrl = baseUrl;
    this.bearerToken = bearerToken;
  }

  get options(): Object { return { headers: { Authorization: this.bearerToken } }; }

  // Base
  close() { this.isConnected = false; }
  connect(): Promise<void> { return this.isConnected ? Promise.resolve() : fetch(`${this.baseUrl}/connection`).then(() => { this.isConnected = true; }); }

  // Analysis
  getCausesOfDeaths(stratifications: String[], filters: String[], yearWindow: number): Promise<CPack> { return Promise.resolve(null); }
  getCorrelation(srcIndicator: String, dstIndicator: String): Promise<CPack> { return Promise.resolve(null); }

  // App
  listExperiments(): Promise<CPack> { return Promise.resolve(null); }
  listRoles(): Promise<CPack> { return Promise.resolve(null); }

  // Discovery
  getCategorizedConceptsByFramework(framework: String): Promise<CPack> { return Promise.resolve(null); }
  getConceptDetails(indicator: String): Promise<CPack> { return Promise.resolve(null); }
  getConceptSynonyms(ids: String[]): Promise<CPack> { return Promise.resolve(null); }
  listDimensions(): Promise<CPack> { return Promise.resolve(null); }

  // Docs
  getByHash(hash: String): Promise<PDF> { return Promise.resolve(null); }
  getLatest(shortForm: String, locale: String): Promise<PDF> { return Promise.resolve(null); }
  listDocuments(): Promise<CPack> { return Promise.resolve(null); }

  // Graph
  getGraph(): Promise<CPack> { return Promise.resolve(null); }

  // Indicator
  getIndicator({ ind, str = [], fil = [], std = [] }: { ind: String, str: String[], fil: String[], std: String[] }): Promise<CPack> {
    console.assert(this.isConnected, "getIndicator : server is not connected");

    return fetch(`${this.baseUrl}/indicator/${ind}?${[].concat(fil.map(s => `f=${s}`), str.map(s => `d=${s}`), std.map(s => `std=${s}`)).join("&")}`, this.options)
        .then(res => res.json())
        .then(json => json['content']['data']);
  }

  // Suggestion
  getDefaultConcept(): Promise<CPack> { return Promise.resolve(null); }
  listConcepts(): Promise<CPack> { return Promise.resolve(null); }

  // User
  getUserByUID(uid: String): Promise<CPack> { return Promise.resolve(null); }
}