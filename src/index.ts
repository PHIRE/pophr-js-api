export interface IAPI {
    // Base
    close();
    connect(): Promise<void>;

    // Analysis
    getCausesOfDeaths(stratifications: String[], filters: String[], yearWindow: number): Promise<CPack>;
    getCorrelation(srcIndicator: String, dstIndicator: String): Promise<CPack>;

    // App
    listExperiments(): Promise<CPack>;
    listRoles(): Promise<CPack>;

    // Discovery
    getCategorizedConceptsByFramework(framework: String): Promise<CPack>;
    getConceptDetails(indicator: String): Promise<CPack>;
    getConceptSynonyms(ids: String[]): Promise<CPack>;
    listDimensions(): Promise<CPack>;

    // Docs
    getByHash(hash: String): Promise<PDF>;
    getLatest(shortForm: String, locale: String): Promise<PDF>;
    listDocuments(): Promise<CPack>;

    // Graph
    getGraph(): Promise<CPack>;

    // Indicator
    getIndicator(config: { ind: String, str: String[], fil: String[], std: String[] }): Promise<CPack>;

    // Suggestion
    getDefaultConcept(): Promise<CPack>;
    listConcepts(): Promise<CPack>;

    // User
    getUserByUID(uid: String): Promise<CPack>;
}

export interface AdminAPI extends IAPI {
    // Feedback
    addFeedback(message: String, isAnonymous: boolean): Promise<CPack>;
    listFeedbacks(): Promise<CPack>;

    // Metrics
    deleteMetric(period: number): Promise<CPack>;
    getByQID(qid: String): Promise<CPack>;
    listMetrics(): Promise<CPack>;
    listMetricsByDetails(period: number, endpoint: String, type: number, isCached: boolean): Promise<CPack>;

    // User
    createUser(user: User): Promise<CPack>;
    listUsers(): Promise<CPack>;
    modifyUser(user: User): Promise<CPack>;
    modifyUserAttr(uid: String, attr: String, value: String): Promise<CPack>;
}

export interface CPack {}
export interface PDF {}
export interface User {}